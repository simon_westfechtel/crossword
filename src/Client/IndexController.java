package Client;

import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.ProgressIndicator;
import javafx.stage.Stage;

public class IndexController implements Controller {
    Client client = null;

    @FXML
    Button submit_button;

    @FXML
    TextField result;
    @FXML
    TextField letter_1;
    @FXML
    TextField letter_2;
    @FXML
    TextField letter_3;
    @FXML
    TextField letter_4;
    @FXML
    TextField letter_5;

    @FXML
    ProgressIndicator progress;

    @FXML
    public void onSubmit(Event event) {

    }

    public void setClientReference (Client client) {
        this.client = client;
    }
}
